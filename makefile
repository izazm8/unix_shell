CC=gcc
CFLAGS= -std=c11 -O0 -Wall -g
INCLUDES=.
LIBS = -lc
SOURCEFILES= functions.c myshellv1.c
OBJS = functions.o myshellv1.o
INSTDIR = /usr/bin


myexe: $(OBJS)
	$(CC) -o myexe $(OBJS)

functions.o: functions.c
	$(CC) -c functions.c 

myshellv1.o: myshellv1.c
	$(CC) -c myshellv1.c

clean:
	-@rm -f $(OBJS) myexe inputIzaz.txt outputIzaz.txt

install: myexe
	@if [ -d $(INSTDIR) ]; \
	then \
		cp myexe $(INSTDIR) && \
		chmod a+x $(INSTDIR)/myexe && \
		chmod og-w $(INSTDIR)/myexe && \
		echo "myexe installed successfully in $(INSTDIR)"; \
	fi

uninstall:
	@rm -f $(INSTDIR)/myexe
	@echo "myexe successfully un-installed from $(INSTDIR)"
