/*
*  Video Lecture: 22
*  Programmer: Arif Butt
*  Course: System Programming with Linux
*  myshellv1.c: 
*  main() displays a prompt, receives a string from keyboard, pass it to tokenize()
*  tokenize() allocates dynamic memory and tokenize the string and return a char**
*  main() then pass the tokenized string to execute() which calls fork and exec
*  finally main() again displays the prompt and waits for next command string
*   Limitations:
*   if user press enter without any input the program gives sigsegv 
*   if user give only spaces and press enter it gives sigsegv
*   if user press ctrl+D it give sigsegv
*   however if you give spaces and give a cmd and press enter it works
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "header.h"

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "IZAZshell:- "
#define MAX_JOBS 20


int main(int argc, char ** argv){

	if(argc==3 && strcmp(argv[1],"-f")==0){
		int fd=open(argv[2],O_RDONLY);
		if(fd<0){
			printf("Error in opening file\n");
			return 0;
		}
		dup2(fd,0);
		close(fd);
	}


   char *cmdline;
   char** arglist;

   char **semicolonSepToken;

   char* prompt = PROMPT;   
   while((cmdline = read_cmd(prompt,stdin)) != NULL){

   		/*
   		if command is starting with ! then read the nummber after that and open the history file and execute the command with the
   		same line no as entered number	

		if it is a command then open file to store the command to keep history

		if the file is full(it contains 10 commands) then create another copy of file and deleted on record and store the new one
		at the top

		*/

   		//this peice of code is concern with history management
   		if(cmdline[0]=='!'){

   			int lineNo;
   			if(cmdline[1]=='-'){
          		if(cmdline[2]=='1' && cmdline[3]=='0'){
            		lineNo=1;
          		}else{
            		int tmp=cmdline[2]-'0';
            		lineNo=10-tmp+1;
          		}
        	} else if (cmdline[1]=='1' && cmdline[2]=='0') {
          		lineNo=10;
        	} else {
          		lineNo=cmdline[1]-'0';
          	}

        
   			free(cmdline);
   			cmdline = (char*) malloc(sizeof(char)*MAX_LEN);

   			FILE* file = fopen("/home/izazm8/SPAssignment/historyFile", "r");
		    int i = 0, check = 0;
		    while (fgets(cmdline, sizeof(char)*MAX_LEN, file)) {
		        i++;
		        if(i == lineNo ){
		        	check = 1;
		            //printf("%s", cmdline);   
		            break;
		        }
		    }
		 
		    fclose(file);

		    if(check==0){
		    	printf("No Command Found..\n");
		    	continue;
		    }
		    char *pos;
			if ((pos=strchr(cmdline, '\n')) != NULL)
    			*pos = '\0';

		    
      	}
      	
      	storeCommandInFile(cmdline);
      	



      	//if there are semicolon separted commands then this piece of code with send thoese commands one by one to execute function
      if((arglist = tokenize(cmdline)) != NULL){
      		semicolonSepToken = allocateMemory();
			int i=0,j=0;
			while(1){
			   	if(arglist[i]==NULL || strcmp(arglist[i],";")==0){					
					
					semicolonSepToken[j]=NULL;

			   		execute(semicolonSepToken);

			   		freeMemory(semicolonSepToken);
			   		semicolonSepToken = allocateMemory();
					j=0;
					if (arglist[i]==NULL)
						break;
					i++;					    		
				}
				int l=0;
				while(arglist[i][l]!='\0')
					l++;
				strncpy(semicolonSepToken[j],arglist[i],l);
				j++;
				if(arglist[i]==NULL)
					break;
				i++;				    	
			}
	         
      }

      freeMemory(arglist);
	  free(cmdline);
  }//end of while loop
   printf("\n");
   return 0;
}
