#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "header.h"

#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "IZAZshell:- "
#define MAX_JOBS 20

struct jobsCommand{
	int pid;
  	int job_id;
  	char command[100];
};


struct jobsCommand bgJobsArray[MAX_JOBS];

int initializeBGJobsArray(){
	for (int i = 0; i < MAX_JOBS; i++) {
      bgJobsArray[i].pid = -1;
    }
}

int execute(char* arglist[]){

	// int l=0;
	// while(1){
	// 	if(arglist[l]==NULL)
	// 		break;

	// 	printf("%s\t", arglist[l]);
	// 	l++;
	// }
	// printf("\n");

	if(executeExternalCommands(arglist)==1){
		return 0;
	}

   int status;

   //int backupInput = dup(0);
   //int backupOuput = dup(1);

   int background=thereIsAndSign(arglist);

   int cpid = fork();

   int tI=-1; //this is used to store the temp value of i, if input file is found

   switch(cpid){
      case -1:
      		perror("fork failed");
	      	exit(1);
      case 0:


      		if(thereArePipes(arglist)==1){
      			runPipedCommands(arglist);
      			return 0;
      		}

      		tI=inputRedirection(arglist);      		
      		ouputRedirection(arglist,tI);  

      		signal(SIGINT, SIG_DFL);
	      	execvp(arglist[0], arglist);
 	      	perror("Command not found...");

      default:

      		signal(SIGINT, SIG_IGN);

      		if(background==0) {
      			signal(SIGCHLD,SIG_DFL);
	     		waitpid(cpid, &status, 0);
	     		signal(SIGINT, SIG_DFL);
      		} else {
      			struct jobsCommand tmp;
		        tmp.pid=cpid;
		        tmp.job_id= getNextJobindex();// get_next_job_id();
		        strcpy(tmp.command,arglist[0]);
		        //tmp.command=arglist;
		        bgJobsArray[tmp.job_id-1]=tmp;

		        printf("[%d] %d\n",tmp.job_id,cpid);
		        signal(SIGCHLD, sigCHLDFunc); 
      		}
	     
	     	//dup2(backupInput,0);
	     	//dup2(backupOuput,1);
         	
         	return 0;
   }
}

//splits array on basis of space
char** tokenize(char* cmdline){
//allocate memory
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
	   arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}      

char* read_cmd(char* prompt, FILE* fp){
   //printf("%s", prompt);
  printf("\033[1;32m%s\033[0m", prompt);
  int c; //input character
   int pos = 0; //position of character in cmdline
   char* cmdline = (char*) malloc(sizeof(char)*MAX_LEN);
   while((c = getc(fp)) != EOF){
       if(c == '\n')
	  break;
       cmdline[pos++] = c;
   }
//these two lines are added, in case user press ctrl+d to exit the shell
   if(c == EOF && pos == 0) 
      return NULL;
   cmdline[pos] = '\0';
   return cmdline;
}


int inputRedirection(char *arglist[]){
	int i=0;
	while(1){
  		if(arglist[i]==NULL)
  			break;

    	if(strcmp(arglist[i],"<")==0){
    		int fd0= open(arglist[i+1], O_RDONLY);
    		if(fd0 < 0){
    			perror("can't open the file");
    			exit(0);
      		}
      		dup2(fd0,0);
      		close(fd0);

      		return i;
 		}
      	i++;

    }
    return -1;
}

void ouputRedirection(char* arglist[], int tI){
	int i=0;
	while(1){
     	if(arglist[i]==NULL)
     		break;

		if(strcmp(arglist[i],">")==0){
			int fd1= open(arglist[i+1], O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
			if(fd1 < 0){
				perror("can't open the file");
				exit(0);
			}
			dup2(fd1,1);
			close(fd1);

			arglist[i]=NULL;
			arglist[i+1]=NULL;
      				
   			break;
      	}
      	i++;

    }
    if(tI!=-1){
  		arglist[tI]=NULL;
   		arglist[tI+1]=NULL;
	}

}

int thereArePipes(char *arglist[]){
	int i=0;
	while(1){
		if(arglist[i]==NULL)
			break;
		if(strcmp(arglist[i],"|")==0)
			return 1;
		i++;
	}
	return -1;
}

void runPipedCommands(char *arglist[]){
      			
    //creating argument list for each pipe
    char** tempArglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
	for(int j=0; j < MAXARGS+1; j++){
		tempArglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
		tempArglist[j]=NULL;
	}

	int check=-1;
	int i=0,j=0;
	while(1){

	   	if(arglist[i]==NULL || strcmp(arglist[i],"|")==0){
			if(check==-1){
				tempArglist[j]=">";
				tempArglist[j+1]="/home/izazm8/SPAssignment/outputIzaz.txt";
				j++;
				check=1;
			}
			else {

				FILE *source;
				source = fopen("/home/izazm8/SPAssignment/outputIzaz.txt", "r");
							
				FILE *target;
				target = fopen("/home/izazm8/SPAssignment/inputIzaz.txt", "w");

				char ch;
				while ((ch = fgetc(source)) != EOF)
					fputc(ch, target);
    			fclose(source);
   				fclose(target);


				tempArglist[j]="<";
				tempArglist[j+1]="/home/izazm8/SPAssignment/inputIzaz.txt";
							
				if(arglist[i]!=NULL){
					tempArglist[j+2]=">";
					tempArglist[j+3]="/home/izazm8/SPAssignment/outputIzaz.txt";
				}

				j=j+3;


			}			    		

			execute(tempArglist);

			for(int k=0;k<=j;k++)
		   		tempArglist[k]=NULL;

			j=0;
			i++;
			    		
		}
		
		tempArglist[j]=arglist[i];
		j++;
		if(arglist[i]==NULL) 
			break;
		i++;
			    	
	}


	//freeing memory
	// for(int k=0; k < MAXARGS+1; k++){
	// 	free(arglist[k]);
	// }
 //    free(arglist);      		
    


}

void freeMemory(char *arglist[]){
	for(int k=0; k < MAXARGS+1; k++){
		free(arglist[k]);
	}
    free(arglist);
}

char** allocateMemory(){
	char** tempArglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
	for(int j=0; j < MAXARGS+1; j++){
		tempArglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
		bzero(tempArglist[j],ARGLEN);
	}
	return tempArglist;

}

int thereIsAndSign(char *arglist[]){
	int i=0;
	while(1){
		if(arglist[i]==NULL)
			return 0;

		if(strcmp(arglist[i],"&")==0){
			arglist[i]=NULL;
			return 1;
		}

		i++;
	}
	
}

void storeCommandInFile(char* arglist){
	
	FILE *f = fopen("/home/izazm8/SPAssignment/historyFile","a");
	fclose(f);

	//counting no of lines in the file
	
	int count_lines=0;
	FILE *fileptr = fopen("/home/izazm8/SPAssignment/historyFile", "r");
    char chr = getc(fileptr);
    while (chr != EOF){
        if (chr == '\n')
            count_lines = count_lines + 1;
        chr = getc(fileptr);
    }
    fclose(fileptr); 



    
    rename("/home/izazm8/SPAssignment/historyFile","/home/izazm8/SPAssignment/historyFile1");
    	
    FILE *outputfp = fopen("/home/izazm8/SPAssignment/historyFile", "a");
   	fprintf(outputfp, "%s\n", &arglist[0]);

   	FILE* file = fopen("/home/izazm8/SPAssignment/historyFile1", "r");
   	char line[256];

    if(count_lines==10){ //limit is met    	
    	
    	int i = 0;
    	int check=0;
    	while (fgets(line, sizeof(line), file)) {
        	i++;
	        if(i == 10 )
	            break;
	        char *pos;
			if ((pos=strchr(line, '\n')) != NULL)
    			*pos = '\0';
	        if(check==0 && strcmp(line,arglist)==0){
	        	continue;
	        }
	        fprintf(outputfp, "%s\n", &line[0]);
	        check=1;
    	}

    } else {
    	int check=0;
    	while (fgets(line, sizeof(line), file)!=NULL) {
    		char *pos;
			if ((pos=strchr(line, '\n')) != NULL)
    			*pos = '\0';
    		if(check==0 && strcmp(line,arglist)==0){
    			continue;
    		}
	        fprintf(outputfp, "%s\n", &line[0]);
	        check=1;
    	}
    }

    remove("/home/izazm8/SPAssignment/historyFile1");

   	fclose(outputfp);
    fclose(file);

}

int getNextJobindex() {
    int i;

    for (i = 0; i < MAX_JOBS; i++) {
        if (bgJobsArray[i].pid == -1) {
            return i+1;
        }
    }

    return -1;
}

void sigCHLDFunc(int signal){
  
  int  pid = wait(NULL);
  int jobid=-1,i;
  for(i=0;i<MAX_JOBS;i++){
   // printf("pid:%d--%d\n",jobsarray[i].pid,jobsarray[i].jobid);
    if(pid==bgJobsArray[i].pid){
        bgJobsArray[i].pid=-1;
        jobid=bgJobsArray[i].job_id;
        break;
      }
  }
  if(jobid==-1)
    return;
  printf("[%d]  %d\n",jobid ,pid);
    

}

int executeExternalCommands(char* arglist[]) 
{ 
	int NoOfOwnCmds = 5, i, switchOwnArg = 0, num; 
	char* ListOfOwnCmds[NoOfOwnCmds]; 
	char* username; 

	ListOfOwnCmds[0] = "exit"; 
	ListOfOwnCmds[1] = "cd"; 
	ListOfOwnCmds[2] = "jobs"; 
	ListOfOwnCmds[3] = "kill"; 
	ListOfOwnCmds[4] = "help"; 

	for (i = 0; i < NoOfOwnCmds; i++) { 
		if (strcmp(arglist[0], ListOfOwnCmds[i]) == 0) { 
			switchOwnArg = i + 1; 
			break; 
		}
	}
	switch (switchOwnArg) { 
		case 1: 
			printf("Goodbye From Izaz Shell\n"); 
			exit(0);
		case 2: 
			//printf("%s\n", arglist[1]);
			chdir(arglist[1]); 
			return 1;
		case 3:
			i=0;
		    for(;i<MAX_JOBS;i++){
		      if(bgJobsArray[i].pid!=-1){
		        printf("[%d] running %s\n",bgJobsArray[i].job_id,bgJobsArray[i].command);
		      }
		    } 
			return 1;
		case 4:
			num=-1;
		    if(arglist[1][0]>='1'&&arglist[1][0]<='9'){
		    	num=arglist[1][0]-'0';
		      	if(arglist[1][1]>='1'&&arglist[1][1]<='9'){
		        	num=num*10;
		        	num+=arglist[1][1]-'0';
		      }
		    }

		    if(num<1|| num>MAX_JOBS || bgJobsArray[num-1].pid==-1){
		    	printf("invalid job id\n");
		      	break; 
		    }
		    kill(bgJobsArray[num-1].pid,SIGKILL);
		    printf("[%d] Killed Process having PID: %d %s\n",num,bgJobsArray[num-1].pid,bgJobsArray[num-1].command);
		    
		    bgJobsArray[num-1].pid=-1;

			return 1;
		case 5: 
			printf("Exit: You can exit your terminal\n");
			printf("CD: You can change your present working directory\n");
			printf("JOBS: You can see which processes are running in background\n");
			printf("KILL: You can kill the process in the background with this command\n");
			printf("HELP: View Help about built in commands\n");
			return 1;  
			
		default: 
			break; 
	} 
	return 0; 
}